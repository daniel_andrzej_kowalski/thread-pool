﻿using Logic.Creators;
using Logic.Fillers;
using System.Linq;
using System.Security.Policy;
using System.Threading;
using ThreadPool.ViewModels;
using Xunit;

namespace UnitTests.Gui
{
    public class MainViewModelTests
    {
        private readonly MainViewModel _mainViewModel;

        public MainViewModelTests()
        {
            var drawingTaskCreator = new DrawingTasksCreator();
            var taskManagerCreator = new TaskManagerFiller(drawingTaskCreator);
            _mainViewModel = new MainViewModel(taskManagerCreator);
        }


        [Fact]
        public void StartStoppingDrawingPoints_Test()
        {
            var drawingTaskCreator = new DrawingTasksCreator();
            var taskManagerCreator = new TaskManagerFiller(drawingTaskCreator);
            var mainViewModel = new MainViewModel(taskManagerCreator);

            var firstCount = mainViewModel.Points.Count;

            mainViewModel.StartCommand.Execute(null);

            Thread.Sleep(5000);

            var secondCount = mainViewModel.Points.Count;

            mainViewModel.StopCommand.Execute(null);

            Thread.Sleep(5000);

            var thirdCount = mainViewModel.Points.Count;

            Assert.True(secondCount > firstCount);
            Assert.Equal(secondCount, thirdCount);
        }

        
        [Fact]
        public void ConfigEditingDisableAfterStartDrawing_Test()
        {
            _mainViewModel.StartCommand.Execute(null);

            Assert.False(_mainViewModel.Configuration.CanBeEdited);

            _mainViewModel.StopCommand.Execute(null);
        }

        [Fact]
        public void ConfigEditingEnabledAfterStopDrawing_Test()
        {
            _mainViewModel.StartCommand.Execute(null);
            _mainViewModel.StopCommand.Execute(null);

            Thread.Sleep(1000);

            Assert.True(_mainViewModel.Configuration.CanBeEdited);
        }

        [Fact]
        public void AfterStartDrawingIsInProgress_Test()
        {
            _mainViewModel.StartCommand.Execute(null);

            Assert.True(_mainViewModel.IsDrawingInProgress);

            _mainViewModel.StopCommand.Execute(null);
        }

        [Fact]
        public void AfterStopDrawingIsNotInProgress_Test()
        {
            _mainViewModel.StartCommand.Execute(null);
            _mainViewModel.StopCommand.Execute(null);

            Thread.Sleep(1000);

            Assert.False(_mainViewModel.IsDrawingInProgress);
        }

        [Fact]
        public void ClearingDrawedPoints_Test()
        {
            var drawingTaskCreator = new DrawingTasksCreator();
            var taskManagerCreator = new TaskManagerFiller(drawingTaskCreator);
            var mainViewModel = new MainViewModel(taskManagerCreator);

            mainViewModel.StartCommand.Execute(null);

            Thread.Sleep(5000);

            mainViewModel.StopCommand.Execute(null);

            Assert.NotEmpty(mainViewModel.Points);

            mainViewModel.ClearCommand.Execute(null);

            Assert.Empty(mainViewModel.Points);
        }
    }
}
