﻿using Common.Exceptions;
using Logic.Creators;
using Xunit;

namespace UnitTests.Logic.Creators
{
    public class DrawingTasksCreatorTests
    {
        private readonly DrawingTasksCreator _drawingTasksCreator;

        public DrawingTasksCreatorTests()
        {
            _drawingTasksCreator = new DrawingTasksCreator();
        }

        [Theory]
        [InlineData(1)]
        [InlineData(10)]
        [InlineData(20)]
        public void CreateGoodAmountOfTasks_Test(int numberOfTasks)
        {
            var tasks = _drawingTasksCreator.CreateManyTasks(numberOfTasks);

            Assert.Equal(numberOfTasks, tasks.Length);
        }

        [Fact]
        public void TooSmallNumberOfTasks_ExceptionTest()
        {
            Assert.Throws<TooSmallNumberOfTasks>(() => { _drawingTasksCreator.CreateManyTasks(-1); });
            Assert.Throws<TooSmallNumberOfTasks>(() => { _drawingTasksCreator.CreateManyTasks(0); });
        }

        [Fact]
        public void TooBigNumberOfTasks_ExceptionTest()
        {
            Assert.Throws<TooBigNumberOfTasks>(() => { _drawingTasksCreator.CreateManyTasks(21); });
            Assert.Throws<TooBigNumberOfTasks>(() => { _drawingTasksCreator.CreateManyTasks(12345); });
        }
    }
}
