﻿using Common.Exceptions;
using Common.Models;
using Logic.Creators;
using Logic.Fillers;
using Logic.Managers;
using Logic.Singletons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Xunit;

namespace UnitTests.Logic.Fillers
{
    public class TaskManagerFillerTests
    {
        private TaskManagerFiller _taskManagerFiller;

        public TaskManagerFillerTests()
        {
            _taskManagerFiller = new TaskManagerFiller(new DrawingTasksCreator());
        }

        [Fact]
        public void CreateTaskManagerIfNullGiven_Test()
        {
            TaskManager taskManger = null;

            _taskManagerFiller.FillTaskManagerWithDrawingTasks(1, ref taskManger);

            Assert.NotNull(taskManger);
        }

        [Fact]
        public void FillWithFirstColor_Test()
        {
            TaskManager taskManger = null;

            _taskManagerFiller.FillTaskManagerWithDrawingTasks(1, ref taskManger);

            taskManger.StartAll();

            Thread.Sleep(1000);

            taskManger.CancelAll();

            PointInfoLockQuerySingleton.Get().TryDequeue(out PointInfo item);

            Assert.Equal(ColorsArraySingleton.Get()[0], item.ColorHex);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(10)]
        [InlineData(20)]
        public void FillWithManyColors_Test(int numbersOfTasks)
        {
            var _colors = ColorsArraySingleton.Get().Take(numbersOfTasks).ToArray();

            TaskManager taskManger = null;

            _taskManagerFiller.FillTaskManagerWithDrawingTasks(_colors.Length, ref taskManger);

            taskManger.StartAll();

            Thread.Sleep(100);

            taskManger.CancelAll();

            var colorHash = new HashSet<string>();

            while (PointInfoLockQuerySingleton.Get().TryDequeue(out PointInfo item))
            {
                colorHash.Add(item.ColorHex);
            }

            Assert.Equal(_colors.Length, colorHash.Count);
            Assert.All(colorHash, color => Assert.Contains(color, _colors));
        }

        [Fact]
        public void TooSmallNumberOfTasks_ExceptionTest()
        {
            TaskManager taskManager = null;
            Assert.Throws<TooSmallNumberOfTasks>(() => _taskManagerFiller.FillTaskManagerWithDrawingTasks(-1, ref taskManager));
            Assert.Throws<TooSmallNumberOfTasks>(() => _taskManagerFiller.FillTaskManagerWithDrawingTasks(0, ref taskManager));
        }

        [Fact]
        public void TooBigNumberOfTasks_ExceptionTest()
        {
            TaskManager taskManager = null;
            Assert.Throws<TooBigNumberOfTasks>(() => _taskManagerFiller.FillTaskManagerWithDrawingTasks(21, ref taskManager));
            Assert.Throws<TooBigNumberOfTasks>(() => _taskManagerFiller.FillTaskManagerWithDrawingTasks(1235, ref taskManager));
        }
    }
}
