﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ThreadPool.Models
{
    public class PointInfoModel : BaseModel
    {
        private string _colorHex;
        private double _x;
        private double _y;

        public string ColorHex
        {
            get
            {
                return _colorHex;
            }
            set
            {
                if (value != _colorHex)
                {
                    _colorHex = value;
                    SetProperty();
                }
            }
        }

        public double X
        {
            get
            {
                return _x;
            }
            set
            {
                if (value != _x && value > 0)
                {
                    _x = value;
                    SetProperty();
                }
            }
        }

        public double Y
        {
            get
            {
                return _y;
            }
            set
            {
                if (value != _y && value > 0)
                {
                    _y = value;
                    SetProperty();
                }
            }
        }
    }
}
