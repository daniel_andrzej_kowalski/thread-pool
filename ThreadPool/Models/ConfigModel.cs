﻿namespace ThreadPool.Models
{
    public sealed class ConfigModel : BaseModel
    {
        private int _numberOfThreads;
        private int _drawingSpeed;
        private bool _canBeEdited;

        public int NumberOfThreads
        {
            get
            {
                return _numberOfThreads;
            }
            set
            {
                if (value != _numberOfThreads)
                { 
                    if (value < 1) 
                    {
                        _numberOfThreads = 1;
                    }
                    else if (value > 20)
                    {
                        _numberOfThreads = 20;
                    }
                    else
                    {
                        _numberOfThreads = value;
                    }
                    SetProperty();
                }
            }
        }

        public int DrawingSpeed
        {
            get
            {
                return _drawingSpeed;
            }
            set
            {
                if (value != _drawingSpeed)
                {
                    if (value <= 0)
                    {
                        _drawingSpeed = 0;
                    }
                    else
                    {
                        _drawingSpeed = value;
                    }
                    SetProperty();
                }
            }
        }

        public bool CanBeEdited
        {
            get
            {
                return _canBeEdited;
            }
            set
            {
                if (value != _canBeEdited)
                {
                    _canBeEdited = value;
                    SetProperty();
                }
            }
        }
    }
}
