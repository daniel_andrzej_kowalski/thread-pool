﻿using Logic.Creators;
using Logic.Fillers;
using System.Windows;
using ThreadPool.ViewModels;

namespace ThreadPool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var drawingTaskCreator = new DrawingTasksCreator();
            var taskManagerCreator = new TaskManagerFiller(drawingTaskCreator);

            DataContext = new MainViewModel(taskManagerCreator);
        }

        private void Canvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var mainViewModel = (DataContext as MainViewModel);
            if (mainViewModel.ResizeCommand.CanExecute(null))
            {
                mainViewModel.ResizeCommand.Execute(e);
            }
        }
    }
}
