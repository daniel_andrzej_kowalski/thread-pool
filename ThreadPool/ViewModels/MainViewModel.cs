﻿using Common.Collections;
using Common.Models;
using Logic.Fillers;
using Logic.Managers;
using Logic.Singletons;
using Logic.Threads;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using ThreadPool.Commands;
using ThreadPool.Models;

namespace ThreadPool.ViewModels
{
    public class MainViewModel
    {
        public ConfigModel Configuration { get; set; }
        public ObservableCollection<PointInfo> Points { get; set; }
        public double CanvasWidth { get; set; }
        public double CanvasHeight { get; set; }

        public bool IsDrawingInProgress { 

            get
            {
                if (_drawWorker == null)
                {
                    return false;
                }
                if (_drawWorker.IsBusy && !_drawWorker.CancellationPending)
                {
                    return true;
                }
                return false;
            }
        }

        public ICommand StartCommand
        {
            get
            {
                if (_startCommand == null)
                {
                    _startCommand = new RelayCommand<object>(
                        param => Start(),
                        predict => !IsDrawingInProgress);
                }
                return _startCommand;
            }
        }

        public ICommand StopCommand
        {
            get
            {
                if (_stopCommand == null)
                {
                    _stopCommand = new RelayCommand<object>(
                        param => Stop(), 
                        predict => IsDrawingInProgress);
                }
                return _stopCommand;
            }
        }

        public ICommand ClearCommand
        {
            get
            {
                if (_clearCommand == null)
                {
                    _clearCommand = new RelayCommand<object>(param => Clear());
                }
                return _clearCommand;
            }
        }

        public ICommand ResizeCommand
        {
            get
            {
                if (_resizeCommand == null)
                {
                    _resizeCommand = new RelayCommand<SizeChangedEventArgs>(param =>
                    {
                        CanvasWidth = param.NewSize.Width;
                        CanvasHeight = param.NewSize.Height;
                    });
                }
                return _resizeCommand;
            }
        }

        private ICommand _startCommand;
        private ICommand _stopCommand;
        private ICommand _clearCommand;
        private ICommand _resizeCommand;

        private readonly BackgroundWorker _drawWorker;

        private readonly TaskManagerFiller _taskManagerCreator;

        private TaskManager _taskManager;

        public MainViewModel(TaskManagerFiller taskMangerCreator)
        {
            _taskManagerCreator = taskMangerCreator;
            _taskManager = new TaskManager();

            Configuration = new ConfigModel
            {
                DrawingSpeed = 1,
                NumberOfThreads = 2,
                CanBeEdited = true
            };

            Points = new ObservableCollection<PointInfo>();

            _drawWorker = new BackgroundWorker
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true
            };

            _drawWorker.DoWork += DrawWorker_DoWork;
            _drawWorker.ProgressChanged += DrawWorker_ProgressChanged;
            _drawWorker.RunWorkerCompleted += DrawWorker_RunWorkerCompleted;
        }

        private void DrawWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (_drawWorker.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }

                var points = new List<PointInfo>();

                for (var i = 0; i < Configuration.DrawingSpeed; i++)
                {
                    if (PointInfoLockQuerySingleton.Get().TryDequeue(out PointInfo point))
                    {
                        if (point == null)
                        {
                            continue;
                        }

                        point.X *= CanvasWidth;
                        point.Y *= CanvasHeight;

                        points.Add(point);
                    }
                }

                _drawWorker.ReportProgress(0, points);

                Thread.Sleep(1000);
            }
        }

        private void DrawWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var points = ((List<PointInfo>)e.UserState).ToArray();
            foreach (var point in points)
            {
                Points.Add(point);
            }
        }

        private void DrawWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _drawWorker.Dispose();

            Configuration.CanBeEdited = true;
        }

        private void Start()
        {
            if (!_drawWorker.IsBusy)
            {
                _drawWorker.RunWorkerAsync();

                Configuration.CanBeEdited = false;

                _taskManagerCreator.FillTaskManagerWithDrawingTasks(Configuration.NumberOfThreads, ref _taskManager);

                _taskManager.StartAll();
            }
        }

        private void Stop()
        {
            if (_drawWorker.IsBusy)
            {
                _drawWorker.CancelAsync();
                _taskManager.Clean();
                PointInfoLockQuerySingleton.Get().Trim();
            }
        }

        private void Clear()
        {
            Points.Clear();
            PointInfoLockQuerySingleton.Get().Trim();
        }
    }
}
