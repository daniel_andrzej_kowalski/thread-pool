﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Text;

namespace Common.Collections
{
    public class FasterCollection<T> : ObservableCollection<T>
    {

        public void AddRange(IEnumerable<T> range)
        {
            CheckReentrancy();

            var index = Count;

            foreach (var item in range)
            {
                Items.Add(item);
                OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index));
                index++;
            }

            OnPropertyChanged(new PropertyChangedEventArgs("Count"));
            OnPropertyChanged(new PropertyChangedEventArgs("Item[]"));

        }
    }
}
