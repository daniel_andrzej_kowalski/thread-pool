﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Common.Collections
{
    public class LockQueue<T>
    {
        private readonly object _locker;
        private readonly Queue<T> _queue;

        public LockQueue()
        {
            _locker = new object();
            _queue = new Queue<T>();
        }

        public void Enqueue(T item)
        {
            lock (_locker)
            {
                _queue.Enqueue(item);
            }
        }

        public bool TryDequeue(out T item)
        {
            lock (_locker)
            {
                if (_queue.Count > 0)
                {
                    item = _queue.Dequeue();
                    return true;
                }

                item = default;
                
                return false;
            }
        }

        public void Clear()
        {
            lock (_locker)
            {
                _queue.Clear();
                _queue.TrimExcess();
            }
        }

        public void Trim()
        {
            lock (_locker)
            {
                _queue.TrimExcess();
            }
        }

        public int Count()
        {
            lock (_locker)
            {
                return _queue.Count;
            }
        }
    }
}
