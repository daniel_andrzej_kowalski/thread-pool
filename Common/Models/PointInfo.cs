﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Models
{
    public class PointInfo
    {
        public string ColorHex { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
    }
}
