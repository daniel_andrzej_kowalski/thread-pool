﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Exceptions
{
    public class TooBigNumberOfTasks : Exception
    {
        public TooBigNumberOfTasks(int number) : base($"{number} is too many tasks to create.") { }

    }
}
