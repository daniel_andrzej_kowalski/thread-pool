﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Exceptions
{
    public class TooSmallNumberOfTasks : Exception
    {
        public TooSmallNumberOfTasks(int number) : base($"{number} is too few tasks to create.") { }
    }
}
