﻿using Logic.Creators;
using Logic.Managers;

namespace Logic.Fillers
{
    public class TaskManagerFiller
    {
        private readonly DrawingTasksCreator _drawingTasksCreator;

        public TaskManagerFiller(DrawingTasksCreator drawingTasksCreator)
        {
            _drawingTasksCreator = drawingTasksCreator;
        }

        public void FillTaskManagerWithDrawingTasks(int numberOfDrawingTasks, ref TaskManager taskManager)
        {
            if (taskManager == null)
            {
                taskManager = new TaskManager();
            }

            var drawingTasks = _drawingTasksCreator.CreateManyTasks(numberOfDrawingTasks);

            taskManager.AddMany(drawingTasks);
        }
    }
}
