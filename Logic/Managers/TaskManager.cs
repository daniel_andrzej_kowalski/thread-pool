﻿using Logic.Threads;
using System.Collections.Generic;

namespace Logic.Managers
{
    public class TaskManager
    {
        private readonly List<MyTask> _tasksList;

        public TaskManager()
        {
            _tasksList = new List<MyTask>();
        }

        public void Add(MyTask task)
        {
            _tasksList.Add(task);
        }

        public void AddMany(IEnumerable<MyTask> tasks)
        {
            _tasksList.AddRange(tasks);
        }

        public void Clean()
        {
            CancelAll();
            _tasksList.Clear();
        }

        public void CancelAll()
        {
            _tasksList.ForEach(task => task.Cancel());
        }

        public void StartAll()
        {
            _tasksList.ForEach(task => task.Start());
        }
    }
}
