﻿using Common.Models;
using Logic.Interfaces;
using Logic.Singletons;
using System;

namespace Logic.Strategies
{
    public class DrawStrategy : IDoWorkStrategy
    {
        private readonly string _colorHex;
        private readonly Random _random;

        public DrawStrategy(string colorHex)
        {
            _colorHex = colorHex;
            _random = new Random();
        }

        public void DoWork()
        {
            PointInfoLockQuerySingleton.Get().Enqueue(new PointInfo
            {
                ColorHex = _colorHex,
                X = _random.NextDouble(),
                Y = _random.NextDouble()
            });
        }
    }
}
