﻿using Common.Collections;
using Common.Models;
using Logic.Interfaces;
using System;
using System.Threading;

namespace Logic.Threads
{
    public class MyTask
    {
        private readonly IDoWorkStrategy _doWorkStrategy;
        private bool _cancellationToken;
        private bool _started;

        public MyTask(IDoWorkStrategy doWorkStrategy)
        {
            _doWorkStrategy = doWorkStrategy;
        }

        public void Start()
        {
            if (!_started)
            {
                var thread = new Thread(() =>
                {
                    while (!_cancellationToken)
                    {
                        _doWorkStrategy.DoWork();
                    }
                });

                thread.IsBackground = true;
                thread.Start();

                _started = true;
            }

        }

        public void Cancel()
        {
            _cancellationToken = true;
            _started = false;
        }
    }
}
