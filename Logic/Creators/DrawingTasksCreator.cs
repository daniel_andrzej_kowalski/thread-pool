﻿using Common.Exceptions;
using Logic.Singletons;
using Logic.Strategies;
using Logic.Threads;
using System;
using System.Collections.Generic;

namespace Logic.Creators
{
    public class DrawingTasksCreator
    {

        public MyTask[] CreateManyTasks(int numberOfTasks)
        {
            var tasks = new List<MyTask>();

            if (numberOfTasks < 1)
            {
                throw new TooSmallNumberOfTasks(numberOfTasks);
            }
            if (numberOfTasks > ColorsArraySingleton.Get().Length)
            {
                throw new TooBigNumberOfTasks(numberOfTasks);
            }

            for (int i = 0; i < numberOfTasks; i++)
            {
                tasks.Add(new MyTask(new DrawStrategy(ColorsArraySingleton.Get()[i])));
            }

            return tasks.ToArray();
        }
    }
}
