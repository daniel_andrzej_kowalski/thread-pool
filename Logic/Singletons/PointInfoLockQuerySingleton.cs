﻿using Common.Collections;
using Common.Models;
using System;
using System.Threading;

namespace Logic.Singletons
{
    public sealed class PointInfoLockQuerySingleton : LockQueue<PointInfo>
    {
        private static PointInfoLockQuerySingleton _instance;

        private PointInfoLockQuerySingleton() 
        { }

        public static PointInfoLockQuerySingleton Get()
        {
            if (_instance == null)
            {
                _instance = new PointInfoLockQuerySingleton();
            }

            return _instance;
        }
    }
}
