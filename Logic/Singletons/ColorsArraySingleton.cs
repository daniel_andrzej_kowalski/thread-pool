﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic.Singletons
{
    public sealed class ColorsArraySingleton
    {
        private static readonly object _locker = new object();
        private static string[] _colors;
        private ColorsArraySingleton() 
        {}

        public static string[] Get()
        {
            lock (_locker)
            {
                if (_colors == null)
                {
                    _colors = new string[]
                    {
                        "#f00",
                        "#003366",
                        "#fb8b35",
                        "#c03e1c",
                        "#008000",
                        "#800040",
                        "#800080",
                        "#008080",
                        "#000",
                        "#6fce20",
                        "#96ff00",
                        "#dc21ff",
                        "#0038ff",
                        "#f96f0c",
                        "#00dadd",
                        "#007acc",
                        "#466914",
                        "#0f0",
                        "#1a9988",
                        "#502c17"
                    };
                }

                return _colors;
            }
        }
    }
}
